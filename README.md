# Deamon Service Library
Note : If you want to know how the libraray works internally you can read it
in the
[Documentation](https://gitlab.com/android-deamon/deamon-service-library/tree/master/documentation/documentation.pdf)

## 1. Purpose
This Android library can be used to spawn deamon services, which run in background
and do some action periodically.

## 2. Usage
Note: If you would like to see an example how to use this library take a look at the related
[DeamonService Demo App](https://gitlab.com/android-deamon/deamon-service-demo/tree/master)

### 2.1. Start a service
To start a service, call DeamonService.startService(). You have to give the application
context, a class implementing the interface "Action" for specifying the action to be done
periodically by the service, startup delay and interval time of the action. This Action object
also have to implement getID() which returns an ID number uniquely identifying the service
which runs this action. This means there must not be any other Action implementation returning
the same ID number.

When you want to receive messages
being sent by the deamon service, you should also register an OnQueueEventListener by calling
DeamonService.registerQueueEventListener(). All messages which have been sent before a
listener has been registered will be discarded.

You should ensure that you call
DeamonService.saveServiceInfo before the android app is paused or close to save the
service information, so the services running when the system shuts down will be restarted
after next boot.

### 2.2. Stop a service
For stopping a service you have to call DeamonService.stopService(). The function expects
application context and ID number of the service to stop. This is the number being returned
by getID() of the action object of the service (like described above). This also disables the
service from autostarting after next boot.