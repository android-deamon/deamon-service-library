package com.divae.deamonservicelibrary.service.storage.database;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteStatement;
import android.os.Handler;
import android.os.Looper;

import com.divae.deamonservicelibrary.service.Action;
import com.divae.deamonservicelibrary.service.DeamonService;
import com.divae.deamonservicelibrary.service.PeriodicAction;
import com.divae.deamonservicelibrary.service.ServiceInfo;
import com.divae.deamonservicelibrary.service.storage.StorageDriver;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.math.BigInteger;
import java.util.HashMap;
import java.util.Map;

/**
 * This class is for storing and loading objects to and from the service info database
 */

public class ServiceDatabaseStorageDriver implements StorageDriver<BigInteger, ServiceInfo> {

    private final Context context;

    public ServiceDatabaseStorageDriver(Context context) {
        this.context = context;
    }

    @Override
    public void saveObjects(Map<BigInteger, ServiceInfo> objectsToSave) {
        ServiceDatabaseOpenHelper openHelper = new ServiceDatabaseOpenHelper(context);
        SQLiteDatabase database = openHelper.getWritableDatabase();

        for (BigInteger id : objectsToSave.keySet()){
            ServiceInfo serviceInfo = objectsToSave.get(id);

            database.delete(DatabaseContractV3.Services.TABLE_NAME, null, null);

            SQLiteStatement statement = database.compileStatement(String.format("INSERT INTO %s (%s, %s, %s, %s, %s) VALUES (?, ?, ?, ?, ?)", DatabaseContractV3.Services.TABLE_NAME,
                    DatabaseContractV3.Services.COLUMN_NAME_ID,
                    DatabaseContractV3.Services.COLUMN_NAME_ACTION,
                    DatabaseContractV3.Services.COLUMN_NAME_STARTUP_DELAY,
                    DatabaseContractV3.Services.COLUMN_NAME_INTERVAL,
                    DatabaseContractV3.Services.COLUMN_NAME_STATE));
            statement.bindString(1, serviceInfo.getId().toString());

            ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
            try {
                ObjectOutputStream objectOutputStream = new ObjectOutputStream(outputStream);
                objectOutputStream.writeObject(serviceInfo.getPeriodicAction().getAction());
                objectOutputStream.close();
            } catch (IOException e) {
                throw new RuntimeException(e);
            }
            byte[] action = outputStream.toByteArray();
            statement.bindBlob(2, action);
            statement.bindLong(3, serviceInfo.getStartupDelay());
            statement.bindLong(4, serviceInfo.getInterval());
            statement.bindLong(5, serviceInfo.getState().getId());
            statement.execute();
            database.close();
        }
    }

    @Override
    public Map<BigInteger, ServiceInfo> loadObjects() {
        Map<BigInteger, ServiceInfo> serviceInfoMap = new HashMap<>();
        ServiceDatabaseOpenHelper openHelper = new ServiceDatabaseOpenHelper(context);
        SQLiteDatabase database = openHelper.getWritableDatabase();

        Cursor cursor = database.query(DatabaseContractV3.Services.TABLE_NAME, DatabaseContractV3.Services.getColumnNames(), null, null, null, null, null);

        while (cursor.moveToNext()){
            BigInteger id = new BigInteger(cursor.getString(cursor.getColumnIndexOrThrow(DatabaseContractV3.Services.COLUMN_NAME_ID)));
            byte[] action = cursor.getBlob(cursor.getColumnIndexOrThrow(DatabaseContractV3.Services.COLUMN_NAME_ACTION));
            long startupDelay = cursor.getLong(cursor.getColumnIndexOrThrow(DatabaseContractV3.Services.COLUMN_NAME_STARTUP_DELAY));
            long interval = cursor.getLong(cursor.getColumnIndexOrThrow(DatabaseContractV3.Services.COLUMN_NAME_INTERVAL));
            long stateID = cursor.getLong(cursor.getColumnIndexOrThrow(DatabaseContractV3.Services.COLUMN_NAME_STATE));

            ByteArrayInputStream inputStream = new ByteArrayInputStream(action);
            try {
                ObjectInputStream objectInputStream = new ObjectInputStream(inputStream);
                Action actionObject = (Action) objectInputStream.readObject();
                PeriodicAction periodicAction = new PeriodicAction(new Handler(Looper.getMainLooper()), interval, DeamonService.getBinder(id), actionObject, context);
                ServiceInfo serviceInfo = new ServiceInfo(id, startupDelay, interval, periodicAction, DeamonService.getBinder(id));
                serviceInfo.setState(ServiceInfo.State.findByID(stateID));
                serviceInfoMap.put(id, serviceInfo);
            } catch (IOException | ClassNotFoundException e) {
                throw new RuntimeException(e);
            }
        }

        cursor.close();
        database.close();

        return serviceInfoMap;
    }
}
