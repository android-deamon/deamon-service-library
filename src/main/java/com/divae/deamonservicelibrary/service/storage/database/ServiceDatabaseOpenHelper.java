package com.divae.deamonservicelibrary.service.storage.database;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import java.io.File;

/**
 * This class is used for handling connections to the service info database
 */

public class ServiceDatabaseOpenHelper extends SQLiteOpenHelper {
    private static final int DATABASE_VERSION = 3;
    private static final File databaseFile = new File("services.db");

    public ServiceDatabaseOpenHelper(Context context) {
        super(context, databaseFile.toString(), null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(DatabaseContractV3.Services.getCreationString());
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        if (oldVersion == 1 || oldVersion == 2){
            db.execSQL("DROP TABLE IF EXISTS Services");
            db.execSQL(DatabaseContractV3.Services.getCreationString());
        }
    }
}
