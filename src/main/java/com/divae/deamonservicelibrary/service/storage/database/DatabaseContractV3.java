package com.divae.deamonservicelibrary.service.storage.database;

import android.provider.BaseColumns;

/**
 * This class describes the database structure being used by ServiceDatabaseStorageDriver class
 */

public final class DatabaseContractV3 {
    private DatabaseContractV3(){}

    public static class Services implements BaseColumns{
        /**
         * The following constants contain the table name and the names of all columns.
         */
        public static final String TABLE_NAME = "services";
        public static final String COLUMN_NAME_ID = "id";
        public static final String COLUMN_NAME_ACTION = "action";
        public static final String COLUMN_NAME_STARTUP_DELAY = "startupdelay";
        public static final String COLUMN_NAME_INTERVAL = "interval";
        public static final String COLUMN_NAME_STATE = "state";

        public static String getCreationString(){
            return String.format("CREATE TABLE %s (%s TEXT PRIMARY KEY, %s BLOB, %s INTEGER, %s INTEGER, %s INTEGER)", TABLE_NAME,
                    COLUMN_NAME_ID, COLUMN_NAME_ACTION, COLUMN_NAME_STARTUP_DELAY, COLUMN_NAME_INTERVAL, COLUMN_NAME_STATE);
        }

        public static String[] getColumnNames(){
            return new String[]{
                    COLUMN_NAME_ID,
                    COLUMN_NAME_ACTION,
                    COLUMN_NAME_STARTUP_DELAY,
                    COLUMN_NAME_INTERVAL,
                    COLUMN_NAME_STATE
            };
        }
    }
}
