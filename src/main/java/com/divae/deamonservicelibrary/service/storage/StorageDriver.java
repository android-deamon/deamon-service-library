package com.divae.deamonservicelibrary.service.storage;

import java.util.Map;

/**
 * This class is for handling disk I/O.
 */

public interface StorageDriver<N, T> {
    /**
     * Stores the given objects to a hard disk, where N is
     * an ID identifying an object and T is the object itself.
     * @param objectsToSave The objects to be saved
     */
    void saveObjects(Map<N, T> objectsToSave);

    /**
     * Loads all stored objects from the hard disk
     * @return Map with keys of type N and values of type T,
     * where T is the object stored on the hard disk and
     * N is an ID identifying the object on disk.
     */
    Map<N, T> loadObjects();
}
