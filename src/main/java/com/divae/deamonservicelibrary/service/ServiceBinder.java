package com.divae.deamonservicelibrary.service;

import android.os.Binder;
import android.util.Log;

import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Queue;
import java.util.Set;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.ConcurrentLinkedQueue;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

/**
 * This objects handles communication messages between the DaemonService and related threads
 * (for example the main thread of a GUI).
 */

public class ServiceBinder extends Binder{
    private final String tag = ServiceBinder.class.getSimpleName();
    public interface OnQueueEventListener{
        void onNewMessage(List<Message> messages);
    }

    ServiceBinder(){}

    private final Queue<Message> messageQueue = new ConcurrentLinkedQueue<>();
    private Set<OnQueueEventListener> queueEventListeners = new HashSet<>();
    private final ExecutorService executorService = new ThreadPoolExecutor(1, 1, 0, TimeUnit.SECONDS, new ArrayBlockingQueue<Runnable>(2, true), new ThreadPoolExecutor.DiscardPolicy());

    private static final Object pollingThreadLock = new Object();

    /**
     * This method reads all messages waiting in the queue
     * */
    private List<Message> getMessages(){
        List<Message> messages = new LinkedList<>();
        Message message = messageQueue.poll();
        while (message != null){
            messages.add(message);
            message = messageQueue.poll();
        }
        Log.d(tag, String.format("Messages received: %s", messages));
        return messages;
    }

    /**
     * This method registers a listener which is called when the content of the message queue changes.
     * */
    public void registerQueueEventListener(OnQueueEventListener queueEventListener){
        this.queueEventListeners.add(queueEventListener);
    }

    /**
     * This method is called, when a message shall be sent to the listeners which are listening to
     * queue events. It adds the message to the message queue and shedules a new run of the message
     * collector to the ExecutorService. When the sheduling queue of the ExecutorService is full, the
     * sheduling of new runs of the message collector will be rejected.
     * */
    public void sendMessage(Message message){
        messageQueue.offer(message);
        Log.d(tag, String.format("Message sent: %s", message));

        synchronized (pollingThreadLock){
            //if (pollingThread == null){
                executorService.submit(new Runnable() {
                    @Override
                    public void run() {
                        List<Message> messages = getMessages();
                        for (OnQueueEventListener eventListener : queueEventListeners){
                            eventListener.onNewMessage(messages);
                        }
                    }
                });
            //}
        }
    }
}
