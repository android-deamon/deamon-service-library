package com.divae.deamonservicelibrary.service;

import java.math.BigInteger;

/**
 * This class stores information about services known by the DeamonService class.
 */

public class ServiceInfo {
    private final BigInteger id;
    private final long startupDelay;
    private final long interval;
    private final PeriodicAction periodicAction;
    private final ServiceBinder binder;
    private State state;

    public ServiceInfo(BigInteger id, long startupDelay, long interval, PeriodicAction periodicAction, ServiceBinder binder) {
        this.id = id;
        this.startupDelay = startupDelay;
        this.interval = interval;
        this.periodicAction = periodicAction;
        this.binder = binder;
        this.state = State.STOPPED;
    }

    public enum State {
        STOPPED(0),
        RUNNING(1),
        STARTONBOOT(2);

        private final long id;

        State(long id) {
            this.id = id;
        }

        public long getId(){
            return id;
        }

        public static State findByID(long id){
            for (State state : values()){
                if (state.getId() == id){
                    return state;
                }
            }
            throw new RuntimeException("Invalid ID");
        }
    }

    public void setState(State state){
        this.state = state;
    }

    public BigInteger getId() {
        return id;
    }

    public long getStartupDelay() {
        return startupDelay;
    }

    public long getInterval() {
        return interval;
    }

    public ServiceBinder getBinder() {
        return binder;
    }

    public State getState() {
        return state;
    }

    public PeriodicAction getPeriodicAction() {
        return periodicAction;
    }
}
