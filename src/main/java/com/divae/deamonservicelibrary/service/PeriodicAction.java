package com.divae.deamonservicelibrary.service;

import android.content.Context;
import android.os.Handler;
import android.os.SystemClock;
import android.util.Log;
import android.widget.Toast;

import java.util.Date;
import java.util.Locale;
import java.util.concurrent.atomic.AtomicBoolean;

/**
 * This class represents an action to be done by the DaemonService periodically.
 */

public class PeriodicAction implements Runnable {
    private final String tag;

    private class Flags {
        private AtomicBoolean stop = new AtomicBoolean(false);
    }

    private final long interval;
    private final Handler handler;
    private final ServiceBinder serviceBinder;
    private final Action action;
    private final Thread thread;
    private final ActionThread actionThread;
    private final Context context;

    private final Flags flags = new Flags();
    private final PeriodicAction runnable = this;

    /**
     * This class represents a thread which is spawned by a PeriodicAction object for performing
     * some action. If the action would be done directly from the PeriodicAction object without using
     * a thread, a long running, CPU intensive action could block the main thread which has started
     * the DaemonService.
     * */
    private class ActionThread implements Runnable{
        private final ServiceBinder serviceBinder;

        private class Flags {
            private AtomicBoolean stop = new AtomicBoolean(false);
            private AtomicBoolean nextTimerEventAborted = new AtomicBoolean(false);
        }

        private final ActionThread.Flags flags = new ActionThread.Flags();

        private ActionThread(ServiceBinder serviceBinder) {
            this.serviceBinder = serviceBinder;
        }

        /**
         * This method does some specified action and handles signals sent by a PeriodicAction object.
         * */
        @Override
        public void run() {
            Log.d(tag, new Date() + " Performing action...");
            if (flags.stop.get()){
                Log.d(tag, "Service stopped.");
                handler.post(new Runnable() {
                    @Override
                    public void run() {
                        Toast.makeText(context, "Service stopped.", Toast.LENGTH_LONG).show();
                    }
                });
                return;
            }
            action.perform(serviceBinder, tag);
            if (flags.stop.get()){
                Log.d(tag, "Servie stopped.");
                handler.post(new Runnable() {
                    @Override
                    public void run() {
                        Toast.makeText(context, "Service stopped.", Toast.LENGTH_LONG).show();
                    }
                });
                return;
            }
            if (flags.nextTimerEventAborted.get()){
                //a following timer event was aborted -> reshedule time event to be call immediately
                Log.d(tag, "Next time event was aborted. Resheduled event.");
                sheduleNextAction(0, handler, runnable);
            }
            Log.d(tag, "Finished.");
        }

        public void stop(){
            flags.stop.set(true);
        }

        public void notifyTimerEventAborted(){
            flags.nextTimerEventAborted.set(true);
        }
    }

    public PeriodicAction(Handler handler, long interval, ServiceBinder serviceBinder, Action action, Context context) {
        this.interval = interval;
        this.handler = handler;
        this.serviceBinder = serviceBinder;
        this.action = action;
        this.tag = String.format(Locale.getDefault(), "Service %d", action.getID());
        this.actionThread = new ActionThread(serviceBinder);
        this.thread = new Thread(actionThread);
        this.context = context;
    }

    /**
     * This method is called, when the sheduling of later runs of PeriodicAction shall be canceled.
     */
    public void stop(){
        flags.stop.set(true);
        this.actionThread.stop();
    }

    /**
     * This method is called by a Handler object after some delay from DaemonService start and
     * spawns a background thread, if not already running, for doing an action.
     * If the background thread is already running, it terminates immediately.
     * When a ABORT signal has been received from the DaemonService, it clears sheduled next runs
     * of the action and then terminates, so the background thread can asynchronously terminate
     * after finishing the performed action and does not shedule a new run of the action.
     * */
    @Override
    public void run() {
        if (thread.isAlive()){
            Log.d(tag, "Action from previous timer event is still running. Abort current time event.");
            actionThread.notifyTimerEventAborted();
            return;
        }
        thread.start();

        if (flags.stop.get()){
            Log.d(tag, "Aborting sheduled timer events...");
            handler.removeCallbacks(runnable);
            Log.d(tag, "done.");
            return;
        }
        Log.d(tag, "Sheduling next timer event.");
        sheduleNextAction(interval, handler, runnable);
        if (flags.stop.get()){
            Log.d(tag, "Aborting sheduled timer events...");
            handler.removeCallbacks(runnable);
            Log.d(tag, "done.");
            return;
        }
    }

    private void sheduleNextAction(long delayTime, Handler handler, Runnable nextRunnable) {
        handler.postAtTime(nextRunnable, SystemClock.uptimeMillis() + delayTime);
    }

    public Action getAction() {
        return action;
    }

    public Handler getHandler() {
        return handler;
    }
}
