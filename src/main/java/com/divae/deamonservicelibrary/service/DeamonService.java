package com.divae.deamonservicelibrary.service;

import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.os.Handler;
import android.os.IBinder;
import android.os.Looper;
import android.os.SystemClock;
import android.support.annotation.Nullable;
import android.util.Log;
import android.widget.Toast;

import com.divae.deamonservicelibrary.service.storage.database.ServiceDatabaseStorageDriver;

import java.math.BigInteger;
import java.util.HashMap;
import java.util.Map;

/**
 * This class is for handling start and stop commands to the service
 */

public class DeamonService extends Service {
    private static final String tag = DeamonService.class.getSimpleName();

    private static Map<BigInteger, ServiceInfo> services = null;
    private static final Map<BigInteger, ServiceBinder> serviceBinders = new HashMap<>();

    /**
     * Returrns the binder of the given service. If it not already exists, it will be
     * created.
     * @param serviceID ID of the service which ServiceBinder shall be returned
     * @return The ServiceBinder of the specified service
     */
    public static ServiceBinder getBinder(BigInteger serviceID){
        if (!serviceBinders.containsKey(serviceID)){
            serviceBinders.put(serviceID, new ServiceBinder());
        }
        return serviceBinders.get(serviceID);
    }

    /**
     * Registers a queue event listener to a deamon service
     * @param serviceID ID of the deamon service
     * @param queueEventListener Event listener to be called when the deamon service sends a message
     */
    public static void registerQueueEventListener(BigInteger serviceID, ServiceBinder.OnQueueEventListener queueEventListener){
        getBinder(serviceID).registerQueueEventListener(queueEventListener);
    }

    /**
     * This is called by the startService method of class 'Context'.
     * for a description of the parameters take a look into javadoc of class 'Service'.
     */
    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        return Service.START_STICKY;
    }

    /**
     * Starts a service, when it is not already running
     * @param context The applications's context
     * @param firstActionDelay Delay (in ms) before the service runs a given action for the first time
     * @param actionInterval Specifies (in ms) the interval for doing the action
     * @param action The action to be done periodically by the service
     */
    public static void startService(Context context, long firstActionDelay, long actionInterval, Action action) {
        init(context);
        if (isRunning(action.getID())){
            Log.d(tag, "Service already running.");
            Toast.makeText(context, "Service already running", Toast.LENGTH_LONG).show();
            return;
        }

        Handler handler = new Handler(Looper.getMainLooper());
        PeriodicAction periodicAction = new PeriodicAction(handler, actionInterval, getBinder(action.getID()), action, context);
        ServiceInfo serviceInfo = new ServiceInfo(action.getID(), firstActionDelay, actionInterval, periodicAction, getBinder(action.getID()));
        services.put(action.getID(), serviceInfo);

        context.startService(new Intent(context, DeamonService.class));
        serviceInfo = services.get(action.getID());
        serviceInfo.setState(ServiceInfo.State.RUNNING);

        sheduleFirstActionCall(serviceInfo.getStartupDelay(), serviceInfo.getPeriodicAction().getHandler(), serviceInfo.getPeriodicAction());
        Log.d(tag, "Service started.");
        Toast.makeText(context, "Service started", Toast.LENGTH_LONG).show();
    }

    /**
     * Returns whether the specified service currently is in running state or not.
     * @param serviceID
     * @return True, when the service is in running state, which means that it does
     * its action in the specified interval of time. False else.
     */
    private static boolean isRunning(BigInteger serviceID) {
        if (services.containsKey(serviceID)){
            ServiceInfo deamonService = services.get(serviceID);
            if (deamonService.getState() == ServiceInfo.State.RUNNING){
                return true;
            }
        }
        return false;
    }

    private static void sheduleFirstActionCall(long firstActionDelay, Handler handler, PeriodicAction periodicAction) {
        handler.postAtTime(periodicAction, SystemClock.uptimeMillis() + firstActionDelay);
    }

    /**
     * This is called by the bindService method of class 'Context'.
     * @param intent
     * @return
     */
    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        Log.d(tag, "Service bound.");
        throw new RuntimeException("Not been implemented yet.");
    }

    /**
     * This is called by the stopService method of class 'Context'.
     */
    @Override
    public void onDestroy() {
        super.onDestroy();
    }

    /**
     * Stops a service, if it is running.
     * @param context The application's context.
     * @param serviceID ID of the service to stop
     */
    public static void stopService(Context context, BigInteger serviceID) {
        init(context);
        if (!isRunning(serviceID) || !services.containsKey(serviceID)){
            Log.d(tag, "Service not running.");
            Toast.makeText(context, "Service not running", Toast.LENGTH_LONG).show();
            return;
        }

        Log.d(tag, "Stopping Service...");
        ServiceInfo deamonService = services.get(serviceID);
        PeriodicAction periodicAction = deamonService.getPeriodicAction();
        periodicAction.stop();
        context.stopService(new Intent(context, DeamonService.class));
        deamonService.setState(ServiceInfo.State.STOPPED);
    }

    /**
     * Stores the service to the hard disk. This is necessary to ensure that all
     * services, that currently run, are restarted on next system boot. It should
     * be called no later than when the app is being closed.
     * @param context The application's context
     */
    public static void saveServiceInfo(Context context){
        ServiceDatabaseStorageDriver driver = new ServiceDatabaseStorageDriver(context);
        for (ServiceInfo serviceInfo : services.values()){
            if (serviceInfo.getState() == ServiceInfo.State.RUNNING){
                serviceInfo.setState(ServiceInfo.State.STARTONBOOT);
            }
        }
        Log.d(tag, "Saving service info...");
        driver.saveObjects(services);
        Log.d(tag, "Service info saved successfully.");
    }

    /**
     * Loads the service info. This has to be done at least once after boot before one of
     * the services is going to be started.
     * @param context The application's context
     */
    public static void init(Context context){
        if (services == null){
            ServiceDatabaseStorageDriver driver = new ServiceDatabaseStorageDriver(context);
            services = driver.loadObjects();
        }
    }
}
